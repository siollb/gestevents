var data;
fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${encodeURIComponent(document.getElementById('eventLocationDataDiv').getAttribute("data-eventLocation"))}.json?access_token=pk.eyJ1IjoibmV3ZSIsImEiOiJja2JkdjU1d3IwZm1vMnluc3o1YmFucHppIn0.F4IBjLCXj-Hco_rx47mMpg`)
    .then(response => {
        return response.json()
    })
    .then(data => doMap(data))
    .catch(error => console.error(error));

function doMap(d) {

    mapboxgl.accessToken = 'pk.eyJ1IjoibmV3ZSIsImEiOiJja2JkdjU1d3IwZm1vMnluc3o1YmFucHppIn0.F4IBjLCXj-Hco_rx47mMpg';
    console.log(document.getElementById('eventLocationDataDiv').getAttribute("data-eventLocation"));
    console.log(d);
    var map = new mapboxgl.Map({
        container: 'map',
        pitch: 45,
        center: [d.features[0].center[0], d.features[0].center[1]],
        zoom: 13,
        style: 'mapbox://styles/mapbox/streets-v11',
        antialias: true
    });

    map.addControl(new mapboxgl.NavigationControl());

    var marker = new mapboxgl.Marker()
        .setLngLat([d.features[0].center[0], d.features[0].center[1]])
        .addTo(map);

    // The 'building' layer in the mapbox-streets vector source contains building-height
    // data from OpenStreetMap.
    map.on('load', function () {

        // Insert the layer beneath any symbol layer.
        var layers = map.getStyle().layers;

        var labelLayerId;
        for (var i = 0; i < layers.length; i++) {
            if (layers[i].type === 'symbol' && layers[i].layout['text-field']) {
                labelLayerId = layers[i].id;
                break;
            }
        }

        map.addLayer(
            {
                'id': '3d-buildings',
                'source': 'composite',
                'source-layer': 'building',
                'filter': ['==', 'extrude', 'true'],
                'type': 'fill-extrusion',
                'minzoom': 15,
                'paint': {
                    'fill-extrusion-color': '#aaa',


                    // use an 'interpolate' expression to add a smooth transition effect to the
                    // buildings as the user zooms in
                    'fill-extrusion-height': [
                        'interpolate',
                        ['linear'],
                        ['zoom'],
                        15,
                        0,
                        15.05,
                        ['get', 'height']
                    ],
                    'fill-extrusion-base': [
                        'interpolate',
                        ['linear'],
                        ['zoom'],
                        15,
                        0,
                        15.05,
                        ['get', 'min_height']
                    ],
                    'fill-extrusion-opacity': 0.6
                }
            },
            labelLayerId
        );
    });
}

