# gest'events
Application web permettant à des utilisateurs de s'inscrire à des évènements

# Installation

- git clone https://gitlab.com/siollb/gestevents.git 
    
- Créer une base de données vide nommée "bdd_gestevents"

- Importer le SQL "events.sql" disponible dans le dossier "database"

- Créer un utilisateur MySQL autorisé à accéder à la nouvelle base de données

- Configurer le fichier "db/connect.php" avec les informations de connexion au serveur de base de données

Accédez à l'application via l'url http://localhost/gestevents

# GitLab
GitLab est une applications ***permettant*** de modifier des projets via différent canaux comme par exemple SSH.
Sur ce projet, chaque élèves modifie un *fichier* ou un dossier afin d'améliorer la compréhension globale de l'**application**.

 >Celà demande malgré tout une bonne methodologie de toute la classe pour que chacun maintienne à jour le projet sans empiéter sur le travail de leur camarade.
