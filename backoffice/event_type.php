<!DOCTYPE html>
<html>
<head>
    <title> Créer un nouveau type d'évènements </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- A noter dans la doc technique -->
    <link rel="icon" type="image/png" href="../images/see.svg" />
    <link rel="stylesheet" type="text/css" href="../css/component.css" />
    <link rel="stylesheet" type="text/css" href="../css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../css/demo.css" />
</head>
    <body>
        <div class="container">
        <?php
		header('Content-Type: text/html; charset=utf-8');

                include "../db/connect.php";
                include "./session_verify.php";
        ?>
        <div class="container"><?php include './includes/menu.php'; ?> </div>

            <h1>Ajouter un nouveau type d'évènement</h1>
            <hr>
            <p>Les types permettent de classer les évènements, vous pouvez ajouter ici un nouveau type.</p>
            <form id="signup_form" action="event_type.php" class="was-validated" method="post">
                <div class="form-group" >
                    <input type="text " name="event_type" class="form-control field-border" placeholder="Nom du type de l'évènement"  required>
             </div>
            </form>    
        <div class="form-group">
            <input  value="annuler" type="reset" name="signup_button" class="btn btn-primary py-3 px-5 " required>
        </div>
        <div class="form-group">
            <input  value="Créer" type="submit" name="create_button" class="btn btn-primary py-3 px-5 " required>
        </div>
    </body>
</html>
<?php 
    include "./cssjs/css.php";
    include "./cssjs/js.php";
?>