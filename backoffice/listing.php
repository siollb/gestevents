<<!DOCTYPE html>
<html>
<head>
    <title> Liste des évènements </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- A noter dans la doc technique -->
    <link rel="icon" type="image/png" href="../images/see.svg" />
    <link rel="stylesheet" type="text/css" href="../css/component.css" />
    <link rel="stylesheet" type="text/css" href="../css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../css/demo.css" />
</head>
    <body>
        <div class="container">
        <?php
		header('Content-Type: text/html; charset=utf-8');

                include "../db/connect.php"; // Connectee aux ficheirs 
                include "./session_verify.php";
                ?> <div class="container">
                    <h1>Menu de navigation</h1>    
                    <?php include './includes/menu.php'; ?> </div>
        <?php   
                // Création de la fonction permettant d'afficher "oui" ou "non" selon les données de reservations
                function checkReserv($reserv) {
                    if ($reserv == 1) {
                        echo "Oui";
                    }
                    else{
                        echo "Non";
                    }   
                }
                
                $sql = "SELECT * FROM events";
                $query = mysqli_query($con, $sql);
                echo '<h1>Liste des évènements</h1>';
                echo '<table class="table table-sm table-bordered">
                    <thead class="table-primary">
                        <tr>
                            <td>Nom de l\'évènement</td>
                            <td>Date de début</td>
                            <td>Date de fin</td> 
                            <td>Réservations</td>   
                            <td>Inscrits</td>
                            <td>Actions</td>
                        <tr>
                    </thead>
                    <tbody class="table-info">'
                ;
                
                while($data = mysqli_fetch_assoc($query)){
                    $firstHour = substr($data["heure_debut"], 0, 5);
                    $lastHour = substr($data["heure_fin"], 0, 5);
            ?>

                <tr style="text-align:justify">
                    <td><?php echo $data["event_title"]; ?></td>
                    <td><?php echo $data["start_date"]; ?></td>
                    <td><?php echo $data["last_date"]; ?></td>
                    <td><?php echo checkReserv($data["reservations"])?></td>
                    <td><?php echo ($data["max_participents"])?></td>
                    <td><a href='editEvent.php?id=<?php echo $data["event_id"]; ?>'><img src="../images/pencil.png" width="24" height="24"></a> <!-- Boutons interactifs : éditer, supprimer, participants -->
                    &nbsp<a href='deleteEvent.php?id=<?php echo $data["event_id"]; ?>'><img src="../images/trash_bin.png" width="24" height="24"></a>
                    <a href='participents.php?id=<?php echo $data["event_id"]; ?>'><img src="../images/crowd.png" width="24" height="24"></a>
                </td>
                </tr>
            <?php } ?>
            </tbody>
            </table>
    </body>
</html>
<?php 
    include "./cssjs/css.php";
    include "./cssjs/js.php";
?>
