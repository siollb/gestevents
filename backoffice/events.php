<!DOCTYPE html>
<html lang="fr">
<?php
session_start();
//lien csss
include "cssjs/css.php";
//lien db 
include "../db/connect.php";
?>

<!-- Choisir au moment de l'inscription si la personne concernée est un employé du lycée, un élève ou une personne extérieure -->

<script>
      function choixInscription() {
            var reserv = document.getElementById("reservation");
            var elementSelect = document.getElementById("choixCateg");
            var valeurChoix = elementSelect.value;
            reserv.style.display = "none";

            if (valeurChoix == "reservation") {
                  reserv.style.display = "block";
            }
      }
</script>

<head>
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
      <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- A noter dans la doc technique -->
      <title>ADMINISTRATION | GestEvents </title>
      <link rel="icon" type="image/png" href="../images/cog.svg" />
      <link rel="stylesheet" type="text/css" href="../css/normalize.css" />
      <link rel="stylesheet" type="text/css" href="../css/demo.css" />
      <link rel="stylesheet" type="text/css" href="../css/component.css" />
      <meta charset="utf-8">
      <script src="../js/modernizr.custom.js"></script>
</head>

<body>

      <style>
            .field-border {
                  border-radius: 20px;

            }
      </style>

      <section class="ftco-section event-section ftco-degree-bg">
            <div class="container">
                  <?php include 'includes/menu.php'; ?>
                  <div class="col-md-8" id="signup_msg">
                        <!-- Gestion des alertes de validation du formulaire-->
                  </div>

                  <div class="row">
                        <div class="col-md-9">
                              <h1><img width="25" height="25" src="images/justifylist.svg" />&nbsp
                                    Création d'un nouvel évènement</h1>

                              <form id="signup_form" action="./execEvents.php" class="was-validated" method="post">
                                    <div class="form-group">
                                          <input type="text " name="event_title" class="form-control field-border"
                                                placeholder="Titre" required>
                                    </div>

                                    <div class="form-group">
                                          <textarea name="event_description" class="form-control field-border"
                                                placeholder="Description" rows="4" cols="50" required></textarea>
                                    </div>

                                    <div class="col-md-12">
                                          <div class="row">
                                                <div class="form-group col-md-6">
                                                      <div class="col-auto my-1">
                                                            <input type="date" name="start_date"
                                                                  class="form-control field-border"
                                                                  value="<?php echo date('Y-m-d'); ?>" min="2015-01-01"
                                                                  required><small>Date du début de l'évènement</small>
                                                      </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                      <div class="col-auto my-1">
                                                            <input type="date" name="last_date"
                                                                  class="form-control field-border"
                                                                  value="<?php echo date('Y-m-d'); ?>" min="2015-01-01"
                                                                  required><small>Date de fin de l'évènement</small>
                                                      </div>
                                                </div>
                                          </div>
                                    </div>

                                    <div class="col-md-12">
                                          <div class="row">
                                                <div class="form-group col-md-6">
                                                      <div class="col-auto my-1">
                                                            <input type="time" name="heure_debut"
                                                                  class="form-control field-border" min="00:00"
                                                                  max="24:00" required><small>Heure du début de
                                                                  l'évènement</small>
                                                      </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                      <div class="col-auto my-1">
                                                            <input type="time" name="heure_fin"
                                                                  class="form-control field-border" min="00:00"
                                                                  max="24:00" required><small>Heure de fin de
                                                                  l'évènement</small>
                                                      </div>
                                                </div>
                                          </div>
                                    </div>

                                    <div class="form-group">
                                          <input type="text" name="location" class="form-control field-border"
                                                placeholder="Lieu" required>
                                    </div>

                                    <div class="form-group">
                                          <h5 class="text-primary">Illustration de l'événement</h5>
                                          <!--<form action="./upload_image.php" method="post" enctype="multipart/form-data">-->
                                          <input type="file" name="image">
                                          <!--<input type="submit" value="Upload" name="upload_image">-->
                                          <!--</form>-->

                                    </div>

                                    <div class="col-md-12">
                                          <div class="row">
                                                <div class="form-group col-md-6">
                                                      <div class="col-auto my-1">
                                                            <select class="custom-select mr-lg-2  field-border"
                                                                  id="choixCateg" name="choix"
                                                                  onchange="javascript:choixInscription(this)" required>
                                                                  <option value="aucun" selected>Libre accès</option>
                                                                  <option value="reservation">Réservation</option>
                                                            </select>
                                                      </div>
                                                </div>
                                          </div>
                                          <div class="form-group">
                                                <h5 class="text-primary">Type d'événement</h5>
                                                <div class="col-auto my-1">
                                                      <select class="custom-select mr-lg-2  field-border" id="typeEvent"
                                                            name="type_event" required>
                                                            <?php
                                                            $sqlType = "SELECT * FROM event_type";
                                                            $queryAnswer = mysqli_query($con, $sqlType);
                                                            while ($dataB = mysqli_fetch_array($queryAnswer)) {
                                                                  echo '<option value=' . $dataB['type_id'] . ' selected>' . $dataB['type_title'] . '</option>';
                                                            }
                                                            ?>

                                                      </select>
                                                </div>
                                          </div>
                                    </div>
                                    <br>
                                    <div id="reservation" style="display:none">
                                          <div class="form-group">
                                                <input type="number" name="max_participents"
                                                      class="form-control-number field-border"
                                                      placeholder="Nombres de participants">
                                          </div>

                                          <div class="col-md-12">
                                                <div class="row">
                                                      <div class="form-group col-md-6">
                                                            <div class="col-auto my-1">
                                                                  <input type="date" name="first_reserv"
                                                                        class="form-control field-border"><small>Début
                                                                        de la réservation</small>
                                                            </div>
                                                      </div>
                                                      <div class="form-group col-md-6">
                                                            <div class="col-auto my-1">
                                                                  <input type="date" name="last_reserv"
                                                                        class="form-control field-border"><small>Fin de
                                                                        la réservation</small>
                                                            </div>
                                                      </div>
                                                </div>
                                          </div>
                                    </div>

                                    <div class="form-group">
                                          <input value="Valider" type="submit" name="signup_button"
                                                class="btn btn-primary py-3 px-5 ">
                                    </div>
                              </form>

                        </div>
                  </div>
            </div>
      </section>


      <!-- loader -->
      <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
                  <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
                  <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                        stroke="#F96D00" />
            </svg></div>

      <?php
      include "cssjs/js.php";
      ?>
</body>

</html>