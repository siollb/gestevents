<?php 
	session_start();
	include '../db/connect.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1"> <!-- A noter dans la doc technique -->
	<title>ADMINISTRATION | GestEvents </title>
	<link rel="icon" type="image/png" href="../images/cog.svg" /> 
	<link rel="stylesheet" type="text/css" href="../css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="../css/demo.css" />
	<link rel="stylesheet" type="text/css" href="../css/component.css" />
	<script src="../js/modernizr.custom.js"></script>
</head>
<body>
	
		<div class="container">
		<?php // Inclusion du menu dynamique
			include './includes/menu.php'; 
		?>
 
			<header>
				<div style="margin-left: 20%!important">
					<h1 style='color: green'>Interface d'administration<span>Gestevents</span><h1>
					<?php // Si l'utilisateur n'est pas connecté on le lui signale 
						if(empty($_SESSION["email"]) || !isset($_SESSION["email"])){ echo "<span style='color: red'> Veuillez vous connecter pour administrer Gestevents</span>"; }
					?>
					<br><img src="../images/cub_logo.png" style="margin-left: 10%!important;" class="img-fluid col-lg-5"/>
				</div>
			</header> 
		</div>
		
</body>
</html>