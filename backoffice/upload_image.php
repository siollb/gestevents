<?php
// Vérifiez si le formulaire a été soumis
if (isset($_POST['upload_image'])) {
    // Spécifiez le dossier où l'image doit être téléchargée
    $target_dir = "images/";

    // Spécifiez le chemin du fichier à télécharger
    $target_file = $target_dir . basename($_FILES["image"]["name"]);

    // Vérifiez si l'image est un fichier image réel ou une fausse image
    $check = getimagesize($_FILES["image"]["tmp_name"]);
    if ($check !== false) {
        // Téléchargez le fichier
        if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
            echo "Le fichier " . basename($_FILES["image"]["name"]) . " a été téléchargé avec succès.";
        } else {
            echo "Désolé, une erreur est survenue lors du téléchargement de votre fichier.";
        }
    } else {
        echo "Le fichier n'est pas une image.";
    }
}
?>