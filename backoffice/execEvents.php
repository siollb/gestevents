<!DOCTYPE html>
<html lang="fr">
<?php
include "cssjs/css.php";
include "../db/connect.php";
session_start();
?>

<head>
      <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- A noter dans la doc technique -->
      <title>ADMINISTRATION | GestEvents </title>
      <link rel="icon" type="image/png" href="../images/cog.svg" />
      <link rel="stylesheet" type="text/css" href="../css/normalize.css" />
      <link rel="stylesheet" type="text/css" href="../css/demo.css" />
      <link rel="stylesheet" type="text/css" href="../css/component.css" />
      <script src="../js/modernizr.custom.js"></script>
</head>

<body>
      <section class="ftco-section contact-section ftco-degree-bg">
            <div class="container">
                  <?php include './includes/menu.php'; ?>
                  <div class="col-md-8" id="signup_msg">
                        <!--Alert from signup form-->
                  </div>
                  <div class="row block-9">
                        <div class="col-md-6 pr-md-5">
                              <!--La fonction "addslashes" en PHP sert à insérer un slash afin de ne pas prendre en compte l'élément suivant.  -->
                              <h1>Résumé de l'évènement créé</h1>
                              <div class="texte">
                                    <?php
                                    $event_title = (empty($_POST['event_title'])) ? NULL : addslashes($_POST['event_title']);
                                    $event_description = (empty($_POST['event_title'])) ? NULL : addslashes($_POST['event_description']);
                                    $date_debut = (empty($_POST['start_date'])) ? NULL : $_POST['start_date'];
                                    $date_fin = (empty($_POST['last_date'])) ? NULL : $_POST['last_date'];
                                    $heure_debut = (empty($_POST['heure_debut'])) ? NULL : $_POST['heure_debut'];
                                    $heure_fin = (empty($_POST['heure_fin'])) ? NULL : $_POST['heure_fin'];
                                    $localisation = (empty($_POST['location'])) ? NULL : addslashes($_POST['location']);
                                    $choix_categorie = (empty($_POST['choix'])) ? NULL : addslashes($_POST['choix']);
                                    $participants_max = (empty($_POST['max_participents']) || $_POST['max_participents'] == 0 || $_POST['max_participents'] == null) ? 0 : $_POST['max_participents'];
                                    $debut_reservation = (empty($_POST['first_reserv'])) ? NULL : $_POST['first_reserv'];
                                    $fin_reservation = (empty($_POST['last_reserv'])) ? NULL : $_POST['last_reserv'];
                                    $evenementImage = (empty($_POST['img_link'])) ? NULL : $_POST['img_link'];
                                    $evenementType = (empty($_POST['type_event'])) ? NULL : $_POST['type_event'];
                                    $evenementLien = (empty($_POST['img_link'])) ? NULL : $_POST['img_link'];


                                    echo "Titre : $event_title";
                                    echo "<br>";
                                    echo "<br>";
                                    echo "Description : $event_description";
                                    echo "<br>";
                                    echo "<br>";
                                    echo "Date début : $date_debut";
                                    echo "<br>";
                                    echo "Date fin : $date_fin";
                                    echo "<br>";
                                    echo "<br>";
                                    echo "Heure du début : $heure_debut";
                                    echo "<br>";
                                    echo "Heure de fin : $heure_fin";
                                    echo "<br>";
                                    echo "<br>";
                                    echo "Lieu : $localisation";
                                    echo "<br>";
                                    echo "<br>";

                                    if ($choix_categorie == "aucun") {
                                          echo "Votre événement ne prend pas en compte de réservation.";
                                          $sql = "INSERT INTO events (event_id, event_title, event_description, start_date, last_date, heure_debut, heure_fin, max_participents, first_reserv, last_reserv, location, img_link, type_id) VALUES (NULL, '$event_title', '$event_description', '$date_debut', '$date_fin', '$heure_debut', '$heure_fin', NULL, NULL, NULL, '$localisation', '$evenementImage', '$evenementType')";
                                    } else if ($choix_categorie == "reservation") {
                                          $sql = "INSERT INTO events (event_id, event_title, event_description, start_date, last_date, heure_debut, heure_fin, max_participents, first_reserv, last_reserv, location, img_link, type_id) VALUES (NULL, '$event_title', '$event_description', '$date_debut', '$date_fin', '$heure_debut', '$heure_fin', '$participants_max', '$debut_reservation', '$fin_reservation', '$localisation', '$evenementImage', '$evenementType')";
                                          echo "Type : Réservation";
                                          echo "<br>";
                                          echo "<br>";
                                          if ($participants_max == "") {

                                                echo "Nombre de participants : $participants_max";
                                                echo "<br>";
                                                echo "Début de la réservation : $debut_reservation";
                                                echo "<br>";
                                                echo "Fin de la réservation : $fin_reservation";
                                          }
                                    }
                                    if (mysqli_query($con, $sql)) {
                                          echo '<p>Nous avons bien enregistré votre évènement !</p>';
                                    } else {
                                          echo "<h5 style='color:red;text-align:center'> ERREUR : Votre enregistrement n'a pas fonctionné correctement !</h5>";
                                          echo $sql;
                                    }


                                    if ($_SERVER["REQUEST_METHOD"] == "POST") {
                                          // Vérifie si le fichier a été uploadé sans erreur.
                                          if (isset($_POST['image'])) {
                                                // Spécifiez le dossier où l'image doit être téléchargée
                                                $target_dir = "images/";

                                                // Spécifiez le chemin du fichier à télécharger
                                                $target_file = $target_dir . basename($_FILES["image"]["name"]);

                                                // Vérifiez si l'image est un fichier image réel ou une fausse image
                                                $check = getimagesize($_FILES["image"]["tmp_name"]);
                                                if ($check !== false) {
                                                      // Téléchargez le fichier
                                                      if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                                                            echo "Le fichier " . basename($_FILES["image"]["name"]) . " a été téléchargé avec succès.";
                                                      } else {
                                                            echo "Désolé, une erreur est survenue lors du téléchargement de votre fichier.";
                                                      }
                                                } else {
                                                      echo "Le fichier n'est pas une image.";
                                                }
                                          }
                                    }
                                    ?>

                              </div>
                        </div>
                  </div>
            </div>
      </section>

      <?php
      include "cssjs/js.php";
      ?>
</body>

</html>