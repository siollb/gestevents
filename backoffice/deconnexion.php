<?php
    // Démarre une session PHP ou reprend la session existante
    session_start();
    
    //Vide tous les éléments de la session en réinitialisant le tableau $_SESSION
    //Cela supprime toutes les variables de session (par exemple, les informations de l'utilisateur connecté)
    $_SESSION = array();
    
    //Détruit complètement la session et supprime les informations de session du serveur
    session_destroy();
    
    // Redirige l'utilisateur vers la page "/gestevents" après la déconnexion
    // L'URL peut être ajustée selon le besoin
    header('Location: /gestevents');

    // Arrête l'exécution du script immédiatement après la redirection
    // Cela garantit qu'aucun code supplémentaire n'est exécuté après la redirection
    exit();
?>
    
