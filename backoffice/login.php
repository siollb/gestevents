<?php
    include "../db/connect.php";

    // Deuxième vérification sur le bon remplissage des informations
    if(empty($_POST["mdp"]) || empty($_POST["email"])){     // Si les informations ne sont pas renseignées ou remplient dans les sections spécifiques à la connexion (login) comme par exemple "emai", alors
        echo "                      
			<div class='alert alert-warning'>
				<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><b>Veuillez remplir tous les champs</b>          
			</div>
		";  // afficher un message d'alerte de type warning, indiquant "Veuillez remplir tous les champs"
		exit();
    }
    else{
        // Récupération des variables postées
        $email = $_POST["email"];      // Récupération de l'email
        $mdp = $_POST["mdp"];
        $sql = "SELECT * FROM `user_info` WHERE `email` = '$email'";    // variable sql en rapport avec la sélection de tous les éléments de la table nommée 'user_info, où l'attribut 'email' est égal à la variable $email en php
        $mdpResult = mysqli_query($con, $sql);
        $resultat = mysqli_fetch_array($mdpResult);
        $verifmdp = password_verify($mdp, $resultat['password']);
        // Vérification de l'existence de l'email
        if (!$resultat){    // Si la variable nommée $resultat est différente alors
            ?> <script> alert("L'identifiant ou le mot de passe est eronné !"); </script> <?php     // executer un script (javascript) donnant l'alerte suivante : "L'identifiant ou le mot de passe est eronné !"  
            echo "<script> location.href='./login.php'; </script>"; // afficher avec le lien suivant : './login.php '
        }
        // Vérification de l'existence du mot de passe
        else{
            if ($verifmdp) {    // Si le mot de passe correspond bien au système de vérification de mot de passe (pasword_verify) présent dans la variable intitulée verifmdp, alors ... 
                session_start();        // démarrage de la session
                $_SESSION["email"] = $email;    // mettre l'email dans la partie spécifique
                echo "<script> location.href='./gesteventsadmin.php'; </script>";    // afficher, à l'aide d'un script javascript, le lien suivant './gesteventsadmin.php'
            }
            else {  // sinon
                ?> <script> alert("L'identifiant ou le mot de passe est eronné !"); </script> <?php // (En javascript) émettre l'alerte suivante : "L'identifiant ou le mot de passe est erroné !"
                echo "<script> location.href='./loginForm.php'; </script>"; // afficher le lien suivant : './loginForm.php'
            }
        }
    }
?>