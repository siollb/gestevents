<?php
    session_start();
    $ip_add = getenv("REMOTE_ADDR");
    include "../db/connect.php";
?>
<!DOCTYPE html>
<html lang="fr">
    <div class="container">
    <?php 
        include "cssjs/css.php";

    ?>
    
    <body>

        <style>
            .field-border{
                border-radius:20px;
            }
        </style>

        <section class="ftco-section contact-section ftco-degree-bg">
                <!-- <div class="col-md-8" id="signin_msg">
                        //Alert from signup form
                </div> -->
                <h1 class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><strong>Veuillez vous connecter <br></strong> afin de gérer les évènements de CUB</h1>
                <div class="row block-9">
                    <div class="col-md-6 pr-md-5">
                        <form action="login.php" method="post">
                            <div class="form-group" >
                                <input type="text " name="email" class="form-control field-border" placeholder="Adresse email" required>
                            </div>
                            <div class="form-group">
                                <input type="password" name="mdp" class="form-control field-border" placeholder="Mot de passe" required>
                            </div>
                            <div class="form-group">
                                <input  value="S'authentifier" type="submit" name="signin_button" class="btn btn-primary py-2 px-5 " required>
                                <a href="execRegister.php">Accueil !</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php
                include "cssjs/js.php";
            ?>
    </body>
</html>
