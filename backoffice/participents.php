<!DOCTYPE html>
<html>
    <title> Liste des participants </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- A noter dans la doc technique -->
    <link rel="icon" type="image/png" href="../images/see.svg" /> 
    <link rel="stylesheet" type="text/css" href="../css/component.css" />
    <link rel="stylesheet" type="text/css" href="../css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../css/demo.css" />

    <body>
    <input style="margin: 5px 10px;" value="Retour" type="submit" name="signup_button" class="btn btn-primary py-3 px-5 "onclick="self.location.href='listing.php'" 
            required
            >
        <div class="container">
            <?php
                $event_id = $_GET["id"];
                include "../db/connect.php";
                include "./session_verify.php";
                ?> <div class="container"><?php include '../includes/menu.php'; ?> </div> <?php
                $sql = "SELECT * FROM participent WHERE event_id = '$event_id'";
                $sql2 = "SELECT event_title FROM events WHERE event_id = '$event_id'";
                $title_answer = mysqli_query($con, $sql2);
                $title = mysqli_fetch_assoc($title_answer);
                $answer = mysqli_query($con, $sql);
                if(mysqli_num_rows($answer) > 0) {
                    echo '<h1>Participants de l\'évènement: <strong>'. $title["event_title"] . '</strong></h1>';
                    echo '<table class="table table-sm table-bordered">
                        <thead class="table-primary">
                            <tr>
                                <td>Nom complet</td>
                                <td>Adresse mail</td>
                                <td>Téléphone</td>
                            <tr>
                        </thead>
                        <tbody class="table-info">'
                    ;
                            while($data = mysqli_fetch_assoc($answer)){ ?>
                                <tr>
                                    <td><?php echo $data["fullname"]; ?></td>
                                    <td><?php echo $data["email"]; ?></td>
                                    <td><?php echo $data["Tel"]; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                <?php } else { echo '<h1 style="color:red!important"> Il n\'y a aucun participant pour cet événement'; }?>     
            </body>
        </html>
<?php
    include "./cssjs/css.php";
    include "./cssjs/js.php";
?>
