<body>
  <?php include "header.php"; ?>
  <!-- END nav -->
  <?php include "alert.php"; ?>
  <div class="hero-wrap js-fullheight" style="background: url(./images/bg-dark.jpg) no-repeat!important; background-size:100%!important;">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start"
        data-scrollax-parent="true">
        <div class="col-md-9 ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
        <div>
          <h1 class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><strong>CUB : Les événements <br></strong></h1>

          <div class="browse d-md-flex col-md-12">
            <div class="row">
              <?php
                  $type_query = "SELECT * FROM event_type";
                  $run_query = mysqli_query($con,$type_query);
                  
                  if(mysqli_num_rows($run_query) > 0){
                    $i=0;   
                    while($row = mysqli_fetch_array($run_query)){
                           
                      $type_id = $row["type_id"];
                      $type_title = $row["type_title"];
                      $tag_id=$i++;
                      echo "
                      <span class='d-flex justify-content-center align-items-md-center'><a href='#$tag_id' style='border-radius:20px;margin-bottom:20px;'><i class=''></i>$type_title</a></span>
                                   
                      ";
                    }
                  }
                  ?>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  
  <!--<section class=" ftco-destination">
    <div class="container">
      <div class="row justify-content-start mb-5 pb-3">
        <div class="col-md-7 heading-section ftco-animate">
          <h2 class="mb-4"><strong>Les événements</strong></h2>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="single-slider owl-carousel ftco-animate">
           <?php
              /*$event_query = "SELECT * FROM event_type";
              $run_query = mysqli_query($con,$event_query);
              while($row = mysqli_fetch_array($run_query)){*/
            ?>
            <div class="item">
              <div class="destination">
                <?php // echo '<a href="#" class="img d-flex justify-content-center align-items-center" style="background-image: url(images/' . $row["image"] . '"></a>'?>
              </div>
            </div>
            <?php //} ?>
          </div>
        </div>
      </div>
    </div>
  </section>-->

  <section class="bg-light" id="events">
    <div class="container" id="0">
      <div class="row justify-content-start mb-5 pb-3">
        <div class="col-md-7 heading-section ftco-animate">
          <h2 class="mb-4"><strong>Trouvez</strong> un évènement qui vous intéresse</h2>
        </div>
      </div>
      <div class="row" id="technical">
        <div class="col-md-12 ftco-animate">
          <div id="accordion">
            <div class="row">
              <div class="col-md-12">
                <div id="get_events"></div>
                <?php
                      $event_query = "SELECT * FROM event_type";
                      $run_query1 = mysqli_query($con,$event_query);
                      
                      if(mysqli_num_rows($run_query1) > 0){
                          
                        while($row = mysqli_fetch_array($run_query1)){
                            
                        $type_id = $row["type_id"];
                        $type_title= $row["type_title"];
                        echo " 
                        <div class='card'>
                        <div class='card-header' id='$type_id'>
                               <a class='card-link' data-toggle='collapse'  href='#menu$type_id' aria-expanded='false' aria-controls='menu$type_id'>$type_title<span class='collapsed'><i class='icon-plus-circle'></i></span><span class='expanded'><i class='icon-minus-circle'></i></span></a>
                               </div> 
                               <div id='menu$type_id' class='collapse'>
                               <div class='card-body'>
                                 <div class='row'>";
                                 $run_query2 = mysqli_query($con,"SELECT * FROM events, event_type WHERE events.type_id=event_type.type_id");
                                 if(mysqli_num_rows($run_query2) > 0){
                       
                                 while($row = mysqli_fetch_array($run_query2)){
                                   if($row['type_id']==$type_id){

                                       $locationFinal = $row['location'];
                                       $encode_title = $row['event_title'];
                                       $encode_description = $row['event_description'];
                                   echo "
                                   <div class='col-md-6 col-lg-3 ftco-animate'>
                                   <div class='destination'>
                                    <a href='#' class='img img-2 d-flex justify-content-center align-items-center' style='background-image: url/{$row['img_link']});'>
                                      <div class='d-flex justify-content-center align-items-center'>
                                      </div>
                                    </a>
                                     <div class='text p-3'>
                                       <h3><a href='#'>{$encode_title}</a></h3>
                                       <p>{$encode_description}</p>
                                       <hr>
                                       <p class='bottom-area d-flex'>
                                         <span><i class='icon-map-o'></i> {$locationFinal}<br></span>
                                         <span class='ml-auto'><a href='register.php?event={$row['event_id']}'>S'inscrire</a></span>
                                       </p>
                                     </div>
                                   </div>
                                 </div>";
                                   }
                    
                                 }
                                 }
                                 
                              echo"  
                              </div>
                               </div>
                             </div>
                             </div>
                             ";
                        }
                      }
                      ?>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
