<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
  <div class="container">
    <!--<a class="navbar-brand" href="index.html">CSE2k19</a> Redir vers index, rempalcer le texte par un autre?-->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav"
      aria-expanded="false" aria-label="Toggle navigation">
      <span class="oi oi-menu"></span> Menu
    </button>

    <div class="collapse navbar-collapse" id="ftco-nav">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active"><a href="index.php" class="nav-link">Accueil</a></li>
        <li class="nav-item"><a href="#events" class="nav-link">Évènements</a></li>
   
        <li class="nav-item cta"><a href="./backoffice/loginForm.php" class="nav-link"><span>Connexion</span></a></li>
      </ul>
    </div>
  </div>
</nav>
