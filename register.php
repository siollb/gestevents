<?php
session_start();
$ip_add = getenv("REMOTE_ADDR");
//base de données
include "db/connect.php";
?>
<!DOCTYPE html>
<html lang="en">
<?php include "cssjs/css.php"; ?>

<body>
    <input style="margin: 10px 10px;" value="Retour" type="submit" name="signup_button" class="btn btn-primary"
        onclick="self.location.href='/gestevents/'" ; required>

    <style>

    </style>

    <section class="ftco-section contact-section ftco-degree-bg">
        <div class="container">
            <div class="" id="signup_msg">

                <?php
                $result = mysqli_query($con, "SELECT * FROM events WHERE event_id={$_GET['event']}");

                while ($data = mysqli_fetch_assoc($result)) {
                    $selOrNot = intval($data["event_id"]) == intval($_GET["event"]) ? 'selected' : '';
                    echo ('<h1 class="text-info border border-primary" style="text-align:center"><strong>' . $data["event_title"] . '</strong></h1>');
                    echo ('<h5 style="text-align:justify">' . $data["event_description"] . '</h5>');
                    global $eventLocation;
                    // Vérifier les données de localisation
                    if (
                        isset($data["City"])
                    ) {
                        // Afficher la carte
                        $eventLocation = $data["Adresse"] . ", " . $data["ZIP"] . " " . $data["City"];
                    } else {
                        // Ne pas afficher la carte
                        $eventLocation = " ";
                    }
                }
                $event_id = $_GET['event'];
                ?>
                <hr style="height: 2px!important;color:red;">
            </div>
            <div class="row block-9">
                <div class="col-md-6 pr-md-5">
                    <form id="form_inscription" action="./frontoffice/execRegister.php" method="POST"
                        class="was-validated">
                        <div class="form-group">
                            <input type="text" name="f_name" class="form-control field-border" placeholder="Prénom"
                                required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="l_name" class="form-control field-border" placeholder="Nom"
                                required>
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control field-border"
                                placeholder="Adresse email" required>
                        </div>

                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <div class="col-auto my-1">
                                        <select class="custom-select mr-lg-2  field-border" id="inlineFormCustomSelect"
                                            name="category" required>
                                            <option value="0" disabled selected>Catégorie...</option>
                                            <option value="employe">Employé du lycée</option>
                                            <option value="eleve">Elève</option>
                                            <option value="autre">Autre</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="event" value="<?php echo $event_id ?>" />
                        <input type="hidden" name="formUrl"
                            value="<?= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>" />
                        <div class="form-group">
                            <input value="S'inscrire" type="submit" name="signup_button"
                                class="btn btn-primary py-3 px-5 " required>
                        </div>
                    </form>
                </div>
                <div id='eventLocationDataDiv' data-eventLocation='<?php echo $eventLocation ?>'>
                </div>
                <div class="col-md-6" id="map"></div>
            </div>
        </div>
    </section>

    <?php include "includes/footer.php"; ?>
    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen">
        <svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#F96D00" />
        </svg>
    </div>

    <?php include "cssjs/js.php"; ?>

</body>


</html>
